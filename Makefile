
dev:
	DIRVOL=./temp docker-compose -f docker-compose.yml up

prod:
	DIRVOL=/mnt/nextclouddrive docker-compose -f docker-compose.yml up

prod-d:
	DIRVOL=/mnt/nextclouddrive docker-compose -f docker-compose.yml up -d

build:
	docker-compose -f docker-compose.yml build

build-clean:
	docker-compose -f docker-compose.yml build --no-cache

bash-nginx:
	docker exec -it nginx bash

bash-nextcloud:
	docker exec -it nextcloud bash

bash-db:
	docker exec -it postgres bash