# bVd-NextCloud

install guide for nextcloud in local network on a raspberry pi

source: [nextcloud](https://github.com/nextcloud/docker)

## Security

source :[raspberrypi.org](https://www.raspberrypi.org/documentation/configuration/security.md)

### New user

```bash
#change username
sudo adduser newuser
#add groups
sudo usermod -a -G adm,dialout,cdrom,sudo,audio,video,plugdev,games,users,input,netdev,gpio,i2c,spi newuser
#add sudo auth
sudo su - newuser
```

delete pi user

```bash
sudo pkill -u pi
sudo deluser pi
sudo deluser -remove-home pi
```

force password sudo
`sudo nano /etc/sudoers.d/010_pi-nopasswd` add : `newuser ALL=(ALL) PASSWD: ALL`

### auto update securityfixes

```bash
sudo crontab -e
0 7 * * 1 apt update && apt upgrade -y
```

### impoving SSH security

add ssh keys

```bash
# copy public key from work pc into .ssh/authorized_keys
nano ~/.ssh/authorized_keys
```

On the work pc add the following configs in .ssh/config

```bash
Host nextcloud
	User bvd
    Hostname 192.168.x.x
    IdentityFile ~/.ssh/id_rsa.pub
```

>**Test if the key works before going to the next step**

set only key login

open following file with texteditor
'/etc/ssh/sshd_config'
set the following configs

```bash
PasswordAuthentication no
UsePAM no
ChallengeResponseAuthentication no
PubkeyAuthentication yes
```

restart ssh

```bash
service ssh restart
```

>**Test if ssh work in other terminal**


### firewall

make sure all the ports are closed exect for 22, 80 and 443

```bash
#install ufw
sudo apt install ufw
#enable ufw
sudo ufw enable
#allow local ssh only
sudo ufw allow from 192.168.0.0/24 to any port 22
sudo ufw allow from 192.168.1.0/24 to any port 22
sudo ufw allow http
sudo ufw allow https

sudo ufw status verbose
```

## Install docker & docker-compose

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
sudo usermod -aG docker pi
export PATH=~/.local/bin:$PATH
sudo apt install python3-pip -y
pip3 install docker-compose
sudo reboot  
```

## External harddrive

intensive writing on a sd-card can dammage the card, therefor we use a harddrive instead

```bash
#find drive  
sudo blkid
#create folder
sudo mkdir /mnt/nextclouddrive
#mount hardrive to folder (example /dev/sda1)
sudo mount /dev/sda1 /mnt/nextclouddrive
#change permissions
sudo chmod 777 /mnt/nextclouddrive
#auto mount
sudo blkid
sudo nano /etc/fstab
#add :  
PARTUUID=7dbd2463-01  /mnt/nextclouddrive ext4 defaults 0 2
```

## Setup nextcloud

clone repo in the raspberry pi
create db.env file
run `make prod`

install from command if needed

```bash
su - www-data -s /bin/bash -c 'php /var/www/html/occ maintenance:install \
--database "pgsql" --database-name "nextcloud" --database-user "nextcloud" \
--database-pass "nextcloud" --admin-user "YourNextcloudAdmin" --admin-pass \
"YourNextcloudAdminPasssword"'
```

Rescan files if needed

```bash
su - www-data -s /bin/bash -c 'php /var/www/html/occ files:scan --all'
```

### Example db.env

```bash
POSTGRES_PASSWORD=
POSTGRES_DB=nextcloud
POSTGRES_USER=nextcloud
```

## Setup ssl keys nginx

open shell nginx container
```bash
certbot --nginx
```

in case needed renew cert

```bash
# test dry run first
certbot renew --dry-run

certbot renew
```

